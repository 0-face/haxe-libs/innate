package innate;

import innate.NativeStr;

abstract Str(NativeStrPtr) from NativeStrPtr to NativeStrPtr{

  public inline function new( v : NativeStrPtr ) {
    this = v;
  }

  @:from
  public static inline function fromNativeStr(s: cpp.Reference<NativeStr>) : Str {
    return new Str(NativeStr.create(s));
  }

  @:from
  public static function fromHaxeString( s: String ) : Str {
    return new Str(NativeStr.create(s));
  }

  @:from
  public static inline function fromCString(cString: cpp.ConstCharStar) : Str {
    if( cString == null ) {
      return null;
    }
    return new Str(NativeStr.create(cString));
  }

  @:to
  public inline function getRef() : cpp.Reference<NativeStr> {
    if( this == null ) {
      throw 'Reference is null';
    }
    return this.ref;
  }


  @:to
  public inline function toString() : String {
    if( this == null ) {
      return null;
    }
    return getRef().toString();
  }

  public inline function c_str() : cpp.ConstCharStar {
    if( this == null ) {
      return null;
    }
    return getRef().c_str();
  }

  public inline function equals( other: Str ) : Bool {
    if( other == null || this == null) {
      return this == other;
    }
    return getRef().equals(other.getRef());
  }
}
