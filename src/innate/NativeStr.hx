package innate;

@:unreflective
@:structAccess
@:include('hx/StdString.h')
@:native('hx::StdString')
@:keep
extern class NativeStr
{
    @:native('new hx::StdString')
    @:overload( function():cpp.Pointer<NativeStr> {} )
    @:overload( function(s:String):cpp.Pointer<NativeStr> {} )
    @:overload( function(s:NativeStr):cpp.Pointer<NativeStr> {} )
    @:overload( function(c:cpp.ConstCharStar):cpp.Pointer<NativeStr> {} )
    public static function create(s: String): cpp.Pointer<NativeStr>;


    @:native('hx::StdString')
    @:overload( function():NativeStr {} )
    @:overload( function(s:String):NativeStr {} )
    @:overload( function(s:NativeStr):NativeStr {} )
    @:overload( function(c:cpp.ConstCharStar):NativeStr {} )
    public static function build(s: String): NativeStr;

    public inline function equals( other:cpp.Reference<NativeStr> ) : Bool{
      return compare(other) == 0;
    }
    public function compare( other:cpp.Reference<NativeStr> ) : Int;

    public function c_str()                   : cpp.ConstCharStar;
    public function size()                    : Int;
    public function empty()                   : Bool;
    public function find(s:String)            : Int;
    public function substr(pos:Int, len:Int)  : NativeStr;
    public function toString()                : String;
}

typedef NativeStrPtr = cpp.Pointer<NativeStr>;
