import massive.munit.TestSuite;

import innate.test.unit.NativeStrTest;
import innate.test.unit.StrTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(innate.test.unit.NativeStrTest);
		add(innate.test.unit.StrTest);
	}
}
