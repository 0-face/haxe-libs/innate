package innate.test.unit;

import innate.NativeStr;

import org.hamcrest.Matchers.*;

class NativeStrTest extends TestScenario{
  @steps
  var steps:TestSteps;

  @Test
  @scenario
  public function build_from_string(){
    when().building_from_a_string("Hello World!");
    then()
      .a_non_null_NativeString_should_be_returned();
      and().it_should_not_be_empty();
      and().it_should_have_a_size_of(12);
  }

  @Test
  @scenario
  public function convert_to_string(){
    given().a_native_string_build_from("Hello World!");
    when().converting_it_to_string();
    then().it_should_return_an_equal_string();
  }

  @Test
  @scenario
  public function find_string(){
    given().a_native_string_build_from("0123456");
    when().finding_string("234");
    then().it_should_return_index(2);
  }

  @Test
  @scenario
  public function substring(){
    given().a_native_string_build_from("0123456");
    when().getting_the_substring(2, 5);
    then().it_should_return_a_native_string_equals_to("23456");
  }

  @Test
  @scenario
  public function equals_native_strings(){
    given().a_native_string_build_from("Lorem ipsum");
    given().another_native_string_build_from("Lorem ipsum");
    then().they_should_be_equals();
  }

  @Test
  @scenario
  public function non_equals_native_strings(){
    given().a_native_string_build_from("Lorem ipsum");
    given().another_native_string_build_from("LOREM IPSUM");
    then().they_should_not_be_equals();
  }


}

private class TestSteps extends hxgiven.Steps<TestSteps> {

  var nativeStr:NativeStr;
  var otherNativeStr:NativeStr;
  var capturedNative:NativeStr;
  var expectedString:String;
  var capturedString:String;
  var capturedIndex:Int = -1;

  public function new() {
    super();
  }

  @step
  public function a_native_string_build_from(s: String){
    when().building_from_a_string(s);

    this.expectedString = s;
  }

  @step
  public function another_native_string_build_from(s: String){
    otherNativeStr = NativeStr.build(s);
  }


  @step
  public function building_from_a_string(s: String){
    nativeStr = NativeStr.build(s);
  }

  @step
  public function converting_it_to_string(){
    capturedString = nativeStr.toString();
  }

  @step
  public function finding_string(s: String){
    capturedIndex = nativeStr.find(s);
  }

  @step
  public function getting_the_substring(idx: Int, len: Int){
    capturedNative = nativeStr.substr(idx, len);
  }


  @step
  public function a_non_null_NativeString_should_be_returned(){
    assertThat(nativeStr, is(notNullValue()));
  }

  @step
  public function it_should_not_be_empty(){
    assertThat(nativeStr.empty(), is(false));
  }

  @step
  public function it_should_have_a_size_of(expected: Int){
    assertThat(nativeStr.size(), is(equalTo(expected)));
  }

  @step
  public function it_should_return_an_equal_string(){
    assertThat(capturedString, is(equalTo(expectedString)));
  }

  @step
  public function it_should_return_index(idx: Int){
    assertThat(capturedIndex, is(idx));
  }

  @step
  public function it_should_return_a_native_string_equals_to(expected: String){
    assertThat(capturedNative.toString(), is(equalTo(expected)));
  }

  @step
  public function they_should_be_equals(){
    assertThat(nativeStr.equals(otherNativeStr), is(true));
  }

  @step
  public function they_should_not_be_equals(){
    assertThat(nativeStr.equals(otherNativeStr), is(false));
  }


}
