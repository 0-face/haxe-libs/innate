package innate.test.unit;

import innate.NativeStr;

import org.hamcrest.Matchers.*;

class StrTest extends TestScenario{
  @steps
  var steps:TestSteps;

  @Test
  @scenario
  public function empty_Str(){
    given().a_non_initialized_Str();
    then().it_should_be_null();
  }

  @Test
  @scenario
  public function create_from_NativeStrPointer(){
    when().creating_from_a_native_string_pointer();
    then().they_should_be_the_same_instance();
  }

  @Test
  @scenario
  public function create_from_empty_NativeStrPointer(){
    when().creating_from_an_empty_native_string_pointer();
    then().it_should_be_null();
  }

  @Test
  @scenario
  public function implicit_cast_to_string(){
    given().a_non_empty_Str_instance();
    when().assigning_it_to_some_haxe_string_instance();
    then()
      .the_haxe_string_should_not_be_empty();
      and().the_Str_and_haxe_string_instances_should_be_equals();
  }

  @Test
  @scenario
  public function implicit_cast_from_string(){
    given().a_non_empty_haxe_string();
    when().assigning_that_haxe_string_to_an_Str_instance();
    then().the_Str_and_haxe_string_instances_should_be_equals();
  }

  @Test
  @scenario
  public function implicit_cast_to_native_string(){
    given().a_non_empty_Str_instance();
    when().assigning_it_to_some_native_string_instance();
    then().the_Str_and_native_string_instances_should_be_equals();
  }

  @Test
  @scenario
  public function implicit_cast_from_native_string(){
    given().a_non_empty_native_string();
    when().assigning_that_native_string_to_an_Str_instance();
    then().the_Str_and_native_string_instances_should_be_equals();
  }

  @Test
  @scenario
  public function implicit_cast_from_c_string(){
    given().a_non_empty_c_string();
    when().assigning_that_c_string_to_an_Str_instance();
    then()
      .the_Str_and_c_string_should_have_the_same_value();
      but().the_c_string_should_not_be_the_same_returned_by_the_underlying_std_string();
  }

  @Test
  @scenario
  public function get_underlying_c_string(){
    given().a_non_empty_Str_instance();
    when().getting_the_c_string();
    then()
      .a_non_null_c_string_should_be_returned();
      and().the_c_string_should_be_the_same_returned_by_the_underlying_std_string();
  }

  @Test
  @scenario
  public function get_c_string_from_null_Str(){
    given().an_Str_instance_created_from_a_null_native_string();
    when().getting_the_c_string();
    then().a_null_c_string_should_be_returned();
  }

  @Test
  @scenario
  public function cast_null_str_to_native_string(){
    given().an_Str_instance_created_from_a_null_native_string();
    when().assigning_it_to_some_native_string_instance();
    then().an_exception_should_have_been_thrown();
  }

  @Test
  @scenario
  public function cast_null_str_to_haxe_string(){
    given().an_Str_instance_created_from_a_null_native_string();
    when().assigning_it_to_some_haxe_string_instance();
    then().the_haxe_string_should_be_set_to_null();
    and().no_exception_should_have_been_thrown();
  }

  @Test
  @scenario
  public function cast_null_c_string_to_str(){
    given().a_null_c_string();
    when().assigning_that_c_string_to_an_Str_instance();
    then().it_should_be_null();
  }


  @Test
  @scenario
  public function null_strs_should_be_equals(){
    given().an_Str_instance_created_from_a_null_native_string();
    then().it_should_be_equals_to_null();
  }

  @Test
  @scenario
  public function null_str_should_not_be_equals_to_non_null(){
    given().an_Str_instance_created_from_a_null_native_string();
    given().another_non_empty_string();
    then().they_should_not_be_equals();
  }

}

private class TestSteps extends hxgiven.Steps<TestSteps>{
  var str:Str;
  var anotherStr:Str;
  var nativeStrPtr:NativeStrPtr;
  var nativeStr:NativeStr;
  var haxeString:String;
  var cString:cpp.ConstCharStar;

  var capturedException:Dynamic;

  public function new(){
    super();
  }

  @step
  public function a_non_initialized_Str(){}

  @step
  public function a_non_empty_Str_instance(){
    when().creating_from_a_native_string_pointer();
  }

  @step
  public function a_non_empty_haxe_string(){
    haxeString = "Lorem ipsum";
  }

  @step
  public function a_non_empty_native_string(){
    nativeStr = NativeStr.build("dolor sit amet");
  }

  @step
  public function a_non_empty_c_string(){
    given().a_non_empty_native_string();

    cString = nativeStr.c_str();
  }

  @step
  public function a_null_c_string(){
    cString = null;
  }


  @step
  public function an_Str_instance_created_from_a_null_native_string(){
    str = new Str(null);
  }

  @step
  public function another_non_empty_string(){
    anotherStr = "Example";
  }


  @step
  public function creating_from_a_native_string_pointer(){
    nativeStrPtr = NativeStr.create("Lorem ipsum");
    str = new Str(nativeStrPtr);
  }

  @step
  public function creating_from_an_empty_native_string_pointer(){
    str = new Str(null);
  }

  @step
  public function assigning_it_to_some_haxe_string_instance(){
    try {
      haxeString = str;
    } catch(e:Dynamic) {
      capturedException = e;
    }
  }

  @step
  public function assigning_it_to_some_native_string_instance(){
    try {
      nativeStr = str;
    } catch(e:Dynamic) {
      capturedException = e;
    }
  }

  @step
  public function assigning_that_haxe_string_to_an_Str_instance(){
    str = haxeString;
  }

  @step
  public function assigning_that_native_string_to_an_Str_instance(){
    str = nativeStr;
  }

  @step
  public function assigning_that_c_string_to_an_Str_instance(){
    str = cString;
  }

  @step
  public function getting_the_c_string(){
    cString = str.c_str();
  }


  @step
  public function it_should_be_null(){
    assertThat(str, is(null));
  }

  @step
  public function they_should_be_the_same_instance(){
      assertThat(str, is(theInstance(nativeStrPtr)));
  }

  @step
  public function it_should_be_equals_to_that_string(){
    assertThat(str.equals(nativeStr), is(true));
  }

  @step
  public function the_haxe_string_should_not_be_empty(){
    assertThat(haxeString, is(notNullValue()));
    assertThat(haxeString.length, is(greaterThan(0)));
  }

  @step
  public function the_Str_and_haxe_string_instances_should_be_equals(){
    assertThat(str.toString(), is(equalTo(haxeString)));
  }

  @step
  public function the_Str_and_native_string_instances_should_be_equals(){
    assertThat(str.equals(nativeStr), is(true));
  }

  @step
  public function the_Str_and_c_string_should_have_the_same_value(){
    assertThat(cString, is(notNullValue()));
    assertThat(str.toString(), is(equalTo(cString.toString())));
  }


  @step
  public function an_exception_should_have_been_thrown(){
    assertThat(capturedException, is(notNullValue()));
  }

  @step
  public function no_exception_should_have_been_thrown(){
    assertThat(capturedException, is(null));
  }

  @step
  public function a_non_null_c_string_should_be_returned(){
    assertThat(cString, is(notNullValue()));
  }

  @step
  public function a_null_c_string_should_be_returned(){
    assertThat(cString == null, is(true));
  }

  @step
  public function the_c_string_should_be_the_same_returned_by_the_underlying_std_string(){
    assertThat(cString == str.getRef().c_str(), is(true));
  }

  @step
  public function the_c_string_should_not_be_the_same_returned_by_the_underlying_std_string(){
    assertThat(cString == str.getRef().c_str(), is(false));
  }

  @step
  public function the_haxe_string_should_be_set_to_null(){
    assertThat(haxeString, is(null));
  }

  @step
  public function the_native_string_should_be_empty(){
    assertThat(nativeStr, is(notNullValue()));
    assertThat(nativeStr.empty(), is(true));
  }

  @step
  public function it_should_be_equals_to_null(){
    assertThat(str.equals(null), is(true));
  }

  @step
  public function they_should_not_be_equals(){
    assertThat(str.equals(anotherStr), is(false));
  }

}
